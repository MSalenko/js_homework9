// Теоретичні питання:

// 1. Новий HTML тег на сторінці створюється за допомогою методу document.createElement(tag);
// 2. В першому параметрі функції elem.insertAdjacentHTML вказується місце вставлення HTML відносно elem;
// beforebegin - перед elem, afterbegin – на початку в elem, beforeend – в кінці в elem, afterend - після elem;
// 3. Видаляємо елемент за допомогою методу node.remove().

// Завдання:

function createList(array, target) {
  const parentElement = document.querySelector(`.${target}`);
  if(!parentElement) {
    parentElement = document.body;
  }
  const listOfElements = document.createElement('ul');
  listOfElements.innerText = 'List of elements:';
  parentElement.append(listOfElements);
  array.forEach(el => {
    const li = document.createElement('li');
    li.innerText = `${el}`;
    listOfElements.append(li);
  });
  timer();
}
// Очищаємо сторінку через 3 секунди з посекундним таймером зворотнього відліку
function timer() {
  const timer = document.createElement('span');
  timer.innerText = '3 sec left';
  document.body.append(timer);
    setTimeout(() => {
      timer.innerText = '2 sec left';
      document.body.append(timer);
    }, 1000);
    setTimeout(() => {
      timer.innerText = '1 sec left';
      document.body.append(timer);
    }, 2000);
    setTimeout(() => document.body.innerHTML = '', 3000);
}
// Перевіряємо, працює)
createList(['Dnipro', 'Kyiv', 'Odesa', 'Kharkiv', 'Lviv'], 'listPlaceholder');
